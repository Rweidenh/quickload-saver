This IGB App creates an IGB QuickLoad from the custom genome data in IGB.

### Use this App to:

* Save custom genomes as IGB QuickLoads so you can easily return to your data after closing IGB.

### To run the App:

1. Install the App.
2. Select **File** > **Save Custom Genome to Local QuickLoad Site**.
3. Select a location on your computer to save the QuickLoad.

IGB QuickLoads can be added to IGB by selecting **File** > **Preferences...** and then selecting the **Data Sources** tab and clicking **Add...**